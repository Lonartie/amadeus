﻿// ******************************************************************************************
// Project: Amadeus
// File:    NEAT.h
// Author:  [ ] .
// Created: 2020-08-01
// Changed: 2020-08-21
// ******************************************************************************************

#include "Export.h"
#include "CommonMacros.h"
#include "UTrainer.h"

namespace Amadeus::Network::Trainer
{
	class AD_EXPORT NEAT final : public UTrainer
	{
	public: /*ctors*/
		AD_DEFAULT_CDTORS(NEAT);
		using UTrainer::UTrainer;

	protected: /*virtual methods from UTrainer*/
		void crossover() override;
		void generation() override;
		void mutation() override;
	};
}
