// ******************************************************************************************
// Project: Amadeus
// File:    UTrainer.h
// Author:  [ ] .
// Created: 2020-07-31
// Changed: 2020-08-21
// ******************************************************************************************

#pragma once
#include "Export.h"
#include "CommonMacros.h"
#include "DynamicANN.h"
#include "UnsupervisedTrainingSet.h"

namespace Amadeus::Network::Trainer
{
	class AD_EXPORT UTrainer
	{
	public: /*typedefs*/
		using NetworkType = AI::DynamicANN;
		using NetworkCollection = std::vector<NetworkType>;
		using TrainingSetType = AI::UnsupervisedTrainingSet;

	public: /*ctors*/
		AD_DEFAULT_CDTORS(UTrainer);
		UTrainer(uint64_t size);
		UTrainer(NetworkCollection&& networks);

	public: /*methods*/
		NetworkCollection& networks();
		const NetworkCollection& networks() const;

		uint64_t size() const;
		void setSize(uint64_t size);

		void train(const TrainingSetType& set);

	protected: /*pure virtual methods*/
		virtual void generation() = 0;
		virtual void crossover() = 0;
		virtual void mutation() = 0;

	private: /*methods*/
		void selection(const TrainingSetType& set);
		
	private: /*members*/
		NetworkCollection m_networks = {};
		uint64_t m_size = 0;
		NetworkType* m_first = nullptr;
		NetworkType* m_second = nullptr;
	};
}