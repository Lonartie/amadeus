// ******************************************************************************************
// Project: Amadeus
// File:    DynamicTrainingSample.h
// Author:  [ ] .
// Created: 2020-07-12
// Changed: 2020-08-21
// ******************************************************************************************

#pragma once
#include "Constants.h"
#include "FixedTrainingSample.h"
#include <Dense>

namespace Amadeus::AI
{
	template<>
	struct TrainingSample<Dynamic, Dynamic> final
	{
		Eigen::VectorXf Inputs;
		Eigen::VectorXf Outputs;
	};

	using DynamicTrainingSample = TrainingSample<Dynamic, Dynamic>;
}