// ******************************************************************************************
// Project: Amadeus
// File:    Sigmoid.h
// Author:  [ ] .
// Created: 2020-07-05
// Changed: 2020-08-21
// ******************************************************************************************

#pragma once
#include "ActivationFunction.h"

namespace Amadeus::AI
{
	class Sigmoid final : public ActivationFunction
	{
	public: /*methods*/
		inline float derivative(float input) const override;
		inline float run(float input) const override;
		
	}; 

	/************************************************/
	/**************** IMPLEMENTATION ****************/
	/************************************************/

	inline float Sigmoid::run(const float input) const
	{
		return 1.0F / (1.0F + std::exp(-input));
	}

	inline float Sigmoid::derivative(const float input) const
	{
		const auto s = run(input);
		return s * (1.0F - s);
	}
}
