// ******************************************************************************************
// Project: Amadeus
// File:    FixedOptimizer.h
// Author:  [ ] .
// Created: 2020-07-07
// Changed: 2020-08-21
// ******************************************************************************************

#pragma once
#include "FixedANN.h"
#include "GradientMap.h"

namespace Amadeus::AI
{
	template<int ... N>
	class Optimizer
	{
	public: /*typedefs*/
		AD_MEMORY(Optimizer);
		
	public: /*ctors*/
		virtual ~Optimizer() = default;

	public: /*pure virtual methods*/
		virtual float calculateDeltaWeight(float previousWeight, const GradientMap<N...>& errorMap) = 0;

	public: /*methods*/
		void setNetwork(const typename FixedANN<N...>::SPtr& network);
		FixedANN<N...>&& network();
		const FixedANN<N...>&& network() const;

	private: /*members*/
		typename FixedANN<N...>::SPtr m_network;
		
	};

	template<int ... N>
	using FixedOptimizer = Optimizer<N...>;

	/************************************************/
	/**************** IMPLEMENTATION ****************/
	/************************************************/

	template <int... N>
	void Optimizer<N...>::setNetwork(const typename FixedANN<N...>::SPtr& network)
	{
		m_network = network;
	}

	template <int... N>
	FixedANN<N...>&& Optimizer<N...>::network()
	{
		return *m_network;
	}

	template <int... N>
	const FixedANN<N...>&& Optimizer<N...>::network() const
	{
		return *m_network;
	}	
}
