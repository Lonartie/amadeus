// ******************************************************************************************
// Project: Amadeus
// File:    DynamicPerceptron.cpp
// Author:  [ ] .
// Created: 2020-07-12
// Changed: 2020-08-21
// ******************************************************************************************

#include "DynamicPerceptron.h"
using namespace Amadeus;
using namespace AI;
using namespace Networks;

Perceptron<Dynamic, Dynamic>::Perceptron(const uint64_t inputs, const uint64_t outputs)
	: AI::DynamicANN(2)
	, m_inputs(inputs)
	, m_outputs(outputs)
{
	layer(0).resize(inputs, 1);
	layer(0).setFunc([this](const float x) {return m_sig.run(x); });
	layer(1).resize(1, outputs);
	layer(1).setFunc([this](const float x) {return m_sig.run(x); });
}

void Perceptron<Dynamic, Dynamic>::resize(const uint64_t inputs, const uint64_t outputs)
{
	m_inputs = inputs;
	m_outputs = outputs;
	
	layer(0).resize(inputs, 1);
	layer(0).setFunc([this](const float x) {return m_sig.run(x); });
	layer(1).resize(1, outputs);
	layer(1).setFunc([this](const float x) {return m_sig.run(x); });
}

uint64_t Perceptron<Dynamic, Dynamic>::inputs() const
{
	return m_inputs;
}

uint64_t Perceptron<Dynamic, Dynamic>::outputs() const
{
	return m_outputs;
}
