// ******************************************************************************************
// Project: Amadeus
// File:    MSE.h
// Author:  [ ] .
// Created: 2020-08-23
// Changed: 2020-08-23
// ******************************************************************************************

#pragma once
#include "Export.h"
#include "LossFunction.h"

namespace Amadeus::AI
{
	/// @brief Mean Squared Error
	class AD_EXPORT MSE : public LossFunction
	{
	public: /*ctors*/
		~MSE() override = default;

	public: /*implementation for LossFunction*/
		float calculateError(float target, float output) override;
		float derivative(float target, float output) override;
		
	};
}
