// ******************************************************************************************
// Project: Amadeus
// File:    DynamicLayer.cpp
// Author:  [ ] .
// Created: 2020-07-12
// Changed: 2020-08-21
// ******************************************************************************************

#include "DynamicLayer.h"
using namespace Amadeus;
using namespace AI;

Layer<Dynamic, Dynamic>::Layer(const uint64_t prevSize, const uint64_t size)
	: m_weights(size, prevSize)
	, m_bias(size)
	, m_prevSize(prevSize)
	, m_size(size)
{}

Layer<Dynamic, Dynamic>::Layer(const uint64_t prevSize, const uint64_t size, const float constant)
	: m_prevSize(prevSize)
	, m_size(size)
{
	initConstant(constant);
}

void Layer<Dynamic, Dynamic>::initConstant(const float constant)
{
	m_weights = WeightsType::Constant(m_size, m_prevSize, constant);
	m_bias = BiasType::Constant(m_size, constant);
}

void Layer<Dynamic, Dynamic>::initRandom()
{
	m_weights = WeightsType::Random(m_size, m_prevSize).unaryExpr([](float x) {return x * 4 - 2; });
	m_bias = BiasType::Random(m_size).unaryExpr([](float x) {return x * 4 - 2; });
}

void Layer<Dynamic, Dynamic>::resize(const uint64_t prevSize, const uint64_t size)
{
	m_prevSize = prevSize;
	m_size = size;
	
	m_weights.resize(m_size, m_prevSize);
	m_bias.resize(m_size);
}

uint64_t Layer<Dynamic, Dynamic>::outputs() const
{
	return m_size;
}

uint64_t Layer<Dynamic, Dynamic>::inputs() const
{
	return m_prevSize;
}
