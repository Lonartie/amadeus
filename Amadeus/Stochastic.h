﻿// ******************************************************************************************
// Project: Amadeus
// File:    Stochastic.h
// Author:  [ ] .
// Created: 2020-08-01
// Changed: 2020-08-21
// ******************************************************************************************

#include "DynamicANN.h"
#include "DynamicSTrainer.h"
#include "GradientMap.h"

namespace Amadeus::Network::Trainer
{
	template<int ... N>
	class Stochastic final : public STrainer<N...>
	{
	public: /*typedefs*/
		using NetworkType = typename STrainer<N...>::NetworkType;
		using TrainingSetType = typename STrainer<N...>::TrainingSetType;
		using OptimizerType = typename STrainer<N...>::OptimizerType;
		using LossFunctionType = typename STrainer<N...>::LossFunctionType;
		using TrainingSampleType = typename collections::parameters::from<decltype(std::declval<TrainingSetType>().Samples)>::template element<0>;

	public: /*ctors*/
		using STrainer<N...>::STrainer;
		
	public: /*virtual methods from STrainer<N...>*/
		void train(const TrainingSetType& set) override;

	private:
		void train(const TrainingSampleType& sample);
	};
	
	/************************************************/
	/**************** IMPLEMENTATION ****************/
	/************************************************/

	template <int... N>
	void Stochastic<N...>::train(const TrainingSetType& set)
	{
		for (const TrainingSampleType& sample : set.Samples)
		{
			this->train(sample);
		}
	}

	template <int... N>
	void Stochastic<N...>::train(const TrainingSampleType& sample)
	{
		AI::GradientMap errors = this->calculateErrorMap(sample);
	}
}
