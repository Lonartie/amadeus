// ******************************************************************************************
// Project: Amadeus
// File:    DynamicOptimizer.cpp
// Author:  [ ] .
// Created: 2020-07-12
// Changed: 2020-08-21
// ******************************************************************************************

#include "DynamicOptimizer.h"
using namespace Amadeus;
using namespace AI;

void Optimizer<-1>::setNetwork(const DynamicANN::SPtr& network)
{
	m_network = network;
}

DynamicANN& Optimizer<Dynamic>::network()
{
	return *m_network;
}

const DynamicANN& Optimizer<Dynamic>::network() const
{
	return *m_network;
}
