// ******************************************************************************************
// Project: Amadeus
// File:    ActivationFunction.h
// Author:  [ ] .
// Created: 2020-07-05
// Changed: 2020-08-21
// ******************************************************************************************

#pragma once
#include <memory>

namespace Amadeus::AI
{
	class ActivationFunction
	{
	public: /*typedefs*/
		using SPtr = std::shared_ptr<ActivationFunction>;
		using UPtr = std::unique_ptr<ActivationFunction>;

	public: /*ctors*/
		virtual ~ActivationFunction() = default;

	public: /*pure virtual methods*/
		virtual float run(float input) const = 0;
		virtual float derivative(float input) const = 0;

	};
}