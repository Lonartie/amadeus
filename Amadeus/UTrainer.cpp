// ******************************************************************************************
// Project: Amadeus
// File:    UTrainer.cpp
// Author:  [ ] .
// Created: 2020-07-31
// Changed: 2020-08-21
// ******************************************************************************************

#include "UTrainer.h"
using namespace Amadeus;
using namespace AI;
using namespace Network;
using namespace Trainer;

UTrainer::UTrainer(const uint64_t size)
{
	m_networks.resize(size);
}

UTrainer::UTrainer(NetworkCollection&& networks)
	: m_networks(std::move(networks))
{}

UTrainer::NetworkCollection& UTrainer::networks()
{
	return m_networks;
}

const UTrainer::NetworkCollection& UTrainer::networks() const
{
	return m_networks;
}

uint64_t UTrainer::size() const
{
	return m_size;
}

void UTrainer::setSize(const uint64_t size)
{
	m_networks.resize(size);
	m_size = size;
}

void UTrainer::train(const TrainingSetType& set)
{
	selection(set);
	crossover();
	mutation();
	generation();
}

void UTrainer::selection(const TrainingSetType& set)
{
	auto bestScore = std::numeric_limits<float>::min();
	auto secondBestScore = std::numeric_limits<float>::min();
	NetworkType* bestNetwork = nullptr;
	NetworkType* secondBestNetwork = nullptr;

	for (auto& sample : set.Samples)
	{
		if (sample.Score > bestScore)
		{
			secondBestNetwork = bestNetwork;
			secondBestScore = bestScore;
			bestNetwork = &sample.Network;
			bestScore = sample.Score;
		}
		else if (sample.Score > secondBestScore)
		{
			secondBestNetwork = &sample.Network;
			secondBestScore = sample.Score;
		}
	}

	m_first = bestNetwork;
	m_second = secondBestNetwork;
}
