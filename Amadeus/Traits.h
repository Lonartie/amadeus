// ******************************************************************************************
// Project: Amadeus
// File:    Traits.h
// Author:  [ ] .
// Created: 2020-07-12
// Changed: 2020-08-22
// ******************************************************************************************

#pragma once
#include <Dense>
#include <tuple>

template<typename ... VS>
auto Vector(VS ... vs) -> Eigen::Matrix<float, sizeof...(VS), 1>
{
	std::array<float, sizeof...(VS)> va = {static_cast<float>(vs)...};
	Eigen::Matrix<float, sizeof...(VS), 1> rv;
	for (int i = 0; i < sizeof...(VS); ++i)
		rv[i] = va[i];
	return rv;
}

template<typename First, typename ... Rest>
struct first_type { using type = First; };

template<int First, int ... Rest>
struct first_int { static constexpr int first = First; };

template<int First>
struct first_int<First> { static constexpr int first = First; };

template<int ... Nums>
static constexpr int first_int_v = first_int<Nums...>::first;

template<int First, int ... Rest>
struct last_int { static constexpr int last = last_int<Rest...>::last; };

template<int Last>
struct last_int<Last> { static constexpr int last = Last; };

template<int ... Nums>
static constexpr int last_int_v = last_int<Nums...>::last;

template<int Depth, int Index, int First, int ... Rest>
constexpr int nth_element_f()
{
	if constexpr (Depth == Index) return First;
	else return nth_element_f<Depth + 1, Index, Rest...>();
}

template<int index, int ... Nums>
struct nth_element
{
	static constexpr int element = nth_element_f<0, index, Nums...>();
};

template<int index, int ... Nums>
static constexpr int nth_element_v = nth_element<index, Nums...>::element;

template<int index, int ... Nums>
struct nth_pair
{
	static constexpr int first = nth_element<index, Nums...>::element;
	static constexpr int second = nth_element<index + 1, Nums...>::element;
};

template<int A, int B>
struct are_equal
{
	static constexpr bool equal = A == B;
};

template<int ... N>
struct sequence{};

template<int Depth, int Begin, int Size, int ... ANums, int ... CNums>
constexpr auto range_f(sequence<CNums...> v)
{
	if constexpr (Depth != Size)
	{
		constexpr int currentNum = nth_element<Begin + Depth, ANums...>::element;
		using currentCollection = sequence<CNums..., currentNum>;
		
		return range_f<Depth + 1, Begin, Size, ANums...>(currentCollection());
	}
	else
	{
		return v;
	}
}

template <int Depth, int... ANums, int... CNums>
constexpr auto reverse_f(sequence<CNums...> v)
{
	if constexpr (Depth < sizeof...(ANums))
	{
		constexpr int currentNum = nth_element<Depth, ANums...>::element;
		using currentCollection = sequence<currentNum, CNums...>;
		
		return reverse_f<Depth + 1, ANums...>(currentCollection());
	}
	else
	{
		return v;
	}
}

template<int ... Nums>
struct collection;

template<int ... Nums>
constexpr collection<Nums...> to_collection_f(sequence<Nums...>)
{
	return collection<Nums...>();
}

template<typename Seq>
struct to_collection
{
	using type = decltype(to_collection_f(std::declval<Seq>()));
};

template<typename Seq>
using to_collection_t = typename to_collection<Seq>::type;

template<int...N, template<int...> class Container, int ... CN>
constexpr auto inject_collection_f(sequence<N...>, Container<CN...>) -> Container<N...>
{
	return {};
}

template<typename T, int ... N>
using inject_collection_t = decltype(inject_collection_f(std::declval<sequence<N...>>(), std::declval<T>()));

template<typename N, typename ... T>
constexpr auto inject_tuple_type_front_f(std::tuple<T...>)
{
	return std::tuple<N, T...>();
}

template<int Depth, int ... N, typename T0, int T1, int T2, int ... R>
constexpr auto to_matrix_tuple_f(Eigen::Matrix<T0, T1, T2, R...> c)
{
	using current_pair = nth_pair<Depth, N...>;
	using container_type = Eigen::Matrix<T0, current_pair::first, current_pair::second, R...>;
	
	if constexpr (Depth == sizeof...(N) - 2)
	{
		using tuple_type = std::tuple<container_type>;
		return tuple_type();
	}
	else
	{
		using last_tuple_type = decltype(to_matrix_tuple_f<Depth + 1, N...>(c));
		using tuple_type = decltype(inject_tuple_type_front_f<container_type>(last_tuple_type()));
		return tuple_type();
	}
}

template<typename T, int ... N>
using to_matrix_tuple_t = decltype(to_matrix_tuple_f<0, N...>(std::declval<Eigen::Matrix<T, 1, 1>>()));

template<int ... Nums>
struct collection
{
	static constexpr bool is_ad_collection = true;
	static constexpr int first = first_int_v<Nums...>;
	static constexpr int last = last_int_v<Nums...>;
	
	template<int Index>
	static constexpr int element = nth_element_v<Index, Nums...>;

	static constexpr int size = sizeof...(Nums);

	template<int Index>
	using pair = nth_pair<Index, Nums...>;

	template<typename Scalar>
	using to_matrix_tuple = to_matrix_tuple_t<Scalar, Nums...>;

	template<int Num>
	using inject_back = collection<Nums..., Num>;
	
	template<int Num>
	using inject_front = collection<Num, Nums...>;

	template<int Begin, int Size>
	using subrange = to_collection_t<decltype(range_f<0, Begin, Size, Nums...>(sequence<>()))>;

	using reversed = to_collection_t<decltype(reverse_f<0, Nums...>(sequence<>()))>;

	template<int Size>
	using remove_front = subrange<Size, size - Size>;

	template<int Size>
	using remove_back = subrange<0, size - Size>;

	template<typename T>
	using inject_to = inject_collection_t<T, Nums...>;
};

template<>
struct collection<>
{
	static constexpr bool is_ad_collection = true;
	static constexpr int size = 0;
	
	template<int Num>
	using inject_back = collection<Num>;
	
	template<int Num>
	using inject_front = collection<Num>;

	using reversed = collection<>;
};

using empty_collection = collection<>;

template<typename ... Ts>
struct type_sequence {};

template<typename Type, template<typename...> class Container, typename ... Params>
constexpr auto add_parameter_front_f(Container<Params...> v)
{
	return Container<Type, Params...>();
}

template<typename Type, template<typename...> class Container, typename ... Params>
constexpr auto add_parameter_back_f(Container<Params...> v)
{
	return Container<Params..., Type>();
}

template<typename T, typename Container>
using add_parameter_front_t = decltype(add_parameter_front_f<T>(std::declval<Container>()));

template<typename T, typename Container>
using add_parameter_back_t = decltype(add_parameter_back_f<T>(std::declval<Container>()));

template<int Size, int Depth, typename First, typename ... Rest, typename ... Types>
constexpr auto add_n_parameters_f(type_sequence<Types...> v)
{
	if constexpr (Depth < Size)
	{
		return add_n_parameters_f<Size, Depth + 1, Rest...>(type_sequence<Types..., First>{});
	}
	else
	{
		return v;
	}
}

template<int Size, template<typename...> class Container, typename ... Params>
constexpr auto get_parameter_collection_f(Container<Params...> v)
{
	if constexpr (Size == -1)
	{
		return type_sequence<Params...>{};
	}
	else
	{
		return add_n_parameters_f<Size, 0, Params...>(type_sequence<>{});
	}
}

template<typename ... Ts>
struct parameter_collection;

template<typename ... Params>
constexpr auto to_parameter_collection_f(type_sequence<Params...>)
{
	return parameter_collection<Params...>{};
}

template<typename Seq>
using to_parameter_collection_t = decltype(to_parameter_collection_f(std::declval<Seq>()));

template<typename Type, int Size = -1>
using get_parameter_collection_t = to_parameter_collection_t<decltype(get_parameter_collection_f<Size>(std::declval<Type>()))>;

template<typename ... NewParams, template<typename...> class Container, typename ... OldParams>
constexpr auto inject_params_f(parameter_collection<NewParams...>, Container<OldParams...>)
{
	return Container<NewParams...>{};
}

template<typename From, typename To>
using inject_params_t = decltype(inject_params_f(std::declval<From>(), std::declval<To>()));

template<typename First, typename ... Rest>
constexpr auto remove_front_f(type_sequence<First, Rest...>)
{
	return type_sequence<Rest...>{};
}

template<typename First>
constexpr auto remove_front_f(type_sequence<First>)
{
	return type_sequence<>{};
}

template<typename ... Ts>
using remove_front_t = to_parameter_collection_t<decltype(remove_front_f(std::declval<type_sequence<Ts...>>()))>;

template<typename First, typename ... Rest, typename ... Current>
constexpr auto remove_back_f(type_sequence<Current...> v)
{
	if constexpr (sizeof...(Rest) > 0)
	{
		return remove_back_f<Rest...>(type_sequence<Current..., First>{});
	}
	else
	{
		return v;
	}
}

template<typename ... Ts>
using remove_back_t = to_parameter_collection_t<decltype(remove_back_f<Ts...>(std::declval<type_sequence<>>()))>;

template<int Depth, int Index, typename First, typename ... Rest>
constexpr auto nth_element_type_f()
{
	if constexpr (Depth < Index)
	{
		return nth_element_type_f<Depth + 1, Index, Rest...>();
	}
	else
	{
		return First{};
	}
}

template<int Index, typename ... Ts>
using nth_element_type_t = decltype(nth_element_type_f<0, Index, Ts...>());

template<int Depth, int Begin, int Size, typename ... Ts, typename ... Cs>
constexpr auto range_element_f(type_sequence<Cs...> v)
{
	if constexpr (Depth < Size)
	{
		return range_element_f<Depth + 1, Begin, Size, Ts...>(type_sequence<Cs..., nth_element_type_t<Begin + Depth, Ts...>>{});
	}
	else
	{
		return v;
	}
}

template<int Begin, int Size, typename ... Ts>
using range_element_t = to_parameter_collection_t<decltype(range_element_f<0, Begin, Size, Ts...>(std::declval<type_sequence<>>()))>;

template<int Depth, typename ... Ts, typename ... Cs>
constexpr auto reverse_types_f(type_sequence<Cs...> v)
{
	constexpr int Size = sizeof...(Ts);
	if constexpr (Depth < Size)
	{
		return reverse_types_f<Depth + 1, Ts...>(type_sequence<Cs..., nth_element_type_t<Size - Depth - 1, Ts...>>{});
	}
	else
	{
		return v;
	}
}

template<typename ... Ts>
using reverse_types_t = to_parameter_collection_t<decltype(reverse_types_f<0, Ts...>(std::declval<type_sequence<>>()))>;

template<int Depth, typename From, typename To, typename ... Ts, typename ... Cs>
constexpr auto replace_type_f(type_sequence<Cs...> v)
{
	if constexpr (Depth < sizeof...(Ts))
	{
		using current_type = typename nth_element_type_t<Depth, Ts...>;
		using mapped_type = typename std::conditional_t<std::is_same_v<current_type, From>, To, current_type>;
		return replace_type_f<Depth + 1, From, To, Ts...>(type_sequence<Cs..., mapped_type>{});
	}
	else
	{
		return v;
	}
}

template<typename From, typename To, typename ... Ts>
using replace_type_t = to_parameter_collection_t<decltype(replace_type_f<0, From, To, Ts...>(std::declval<type_sequence<>>()))>;

template<typename ... Ts>
struct parameter_collection
{
	static constexpr int size = sizeof...(Ts);

	template<typename T>
	using inject_front = parameter_collection<T, Ts...>;
	
	template<typename T>
	using inject_back = parameter_collection<Ts..., T>;

	using remove_front = remove_front_t<Ts...>;
	
	using remove_back = remove_back_t<Ts...>;

	template<typename T, int Size = -1>
	using from = get_parameter_collection_t<T, Size>;

	template<typename T>
	using transfer_to = inject_params_t<parameter_collection<Ts...>, T>;

	template<int Index>
	using element = nth_element_type_t<Index, Ts...>;

	template<int Begin, int Size>
	using subrange = range_element_t<Begin, Size, Ts...>;

	template<typename From, typename To>
	using replace = replace_type_t<From, To, Ts...>;
};

template<>
struct parameter_collection<>
{
	static constexpr int size = 0;
	
	template<typename T>
	using inject_front = parameter_collection<T>;
	
	template<typename T>
	using inject_back = parameter_collection<T>;	

	template<typename T, int Size = -1>
	using from = get_parameter_collection_t<T, Size>;
};

struct placeholder{};

using empty_parameter_collection = parameter_collection<>;

struct collections
{
	using parameters = empty_parameter_collection;
	using numericals = empty_collection;
};