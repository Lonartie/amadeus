﻿// ******************************************************************************************
// Project: Amadeus
// File:    MSE.cpp
// Author:  [ ] .
// Created: 2020-08-23
// Changed: 2020-08-23
// ******************************************************************************************

#include "MSE.h"
using namespace Amadeus::AI;

float MSE::calculateError(const float target, const float output)
{
	const auto direction = target - output;
	return .5 * direction * direction;
}

float MSE::derivative(const float target, const float output)
{
	return output - target;
}
