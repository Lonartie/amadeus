// ******************************************************************************************
// Project: Amadeus
// File:    UnsupervisedTrainingSample.h
// Author:  [ ] .
// Created: 2020-07-31
// Changed: 2020-08-21
// ******************************************************************************************

#pragma once
#include "DynamicANN.h"

namespace Amadeus::AI
{
	struct UnsupervisedTrainingSample
	{
		DynamicANN& Network;
		float Score;
	};
}