// ******************************************************************************************
// Project: Amadeus
// File:    LossFunction.h
// Author:  [ ] .
// Created: 2020-07-07
// Changed: 2020-08-21
// ******************************************************************************************

#pragma once
#include "CommonMacros.h"
#include "GradientMap.h"

namespace Amadeus::AI
{
	class LossFunction
	{
	public: /*typedefs*/
		AD_MEMORY(LossFunction);
		
	public: /*ctors*/
		virtual ~LossFunction() = default;

	public: /*pure virtual methods*/
		virtual float calculateError(float target, float output) = 0;
		virtual float derivative(float target, float output) = 0;
		
	};
}