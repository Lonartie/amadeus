// ******************************************************************************************
// Project: Amadeus
// File:    DynamicOptimizer.h
// Author:  [ ] .
// Created: 2020-07-12
// Changed: 2020-08-21
// ******************************************************************************************

#pragma once
#include "Export.h"
#include "Constants.h"
#include "FixedOptimizer.h"
#include "DynamicANN.h"

namespace Amadeus::AI
{
	template<>
	class AD_EXPORT Optimizer<Dynamic>
	{
	public: /*ctors*/
		virtual ~Optimizer() = default;

	public: /*pure virtual methods*/
		virtual float calculateDeltaWeight(float previousWeight, const GradientMap<Dynamic>& errorMap) = 0;

	public: /*methods*/
		void setNetwork(const DynamicANN::SPtr& network);
		DynamicANN& network();
		const DynamicANN& network() const;

	private: /*members*/
		DynamicANN::SPtr m_network;
		
	};

	using DynamicOptimizer = Optimizer<Dynamic>;
}