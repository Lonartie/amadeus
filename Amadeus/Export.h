// ******************************************************************************************
// Project: Amadeus
// File:    Export.h
// Author:  [ ] .
// Created: 2020-07-01
// Changed: 2020-08-21
// ******************************************************************************************

#pragma once

#include <QtCore/qglobal.h>

#ifndef BUILD_STATIC
# if defined(AMADEUS_LIB)
#  define AD_EXPORT Q_DECL_EXPORT
# else
#  define AD_EXPORT Q_DECL_IMPORT
# endif
#else
# define AD_EXPORT
#endif
