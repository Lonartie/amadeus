// ******************************************************************************************
// Project: Amadeus
// File:    DynamicSTrainer.cpp
// Author:  [ ] .
// Created: 2020-07-12
// Changed: 2020-08-21
// ******************************************************************************************

#include "DynamicSTrainer.h"
using namespace Amadeus;
using namespace AI;
using namespace Network;
using namespace Trainer;

STrainer<Dynamic>::STrainer(NetworkType& network)
	: m_network(network)
{}

STrainer<Dynamic>::NetworkType& STrainer<Dynamic>::network()
{
	return m_network;
}

const STrainer<Dynamic>::NetworkType& STrainer<Dynamic>::network() const
{
	return m_network;
}
