// ******************************************************************************************
// Project: Amadeus
// File:    FixedLayers.h
// Author:  [ ] .
// Created: 2020-07-12
// Changed: 2020-08-21
// ******************************************************************************************

#pragma once
#include "FixedLayer.h"
#include "Traits.h"

namespace Amadeus::AI
{
	/* GENERAL IMPLEMENTATION */

	template<int Index, int PrevSize, int ... RestSizes>
	class LayerList: public LayerList<Index + 1, RestSizes...>
	{
		using Base = LayerList<Index + 1, RestSizes...>;

	public: /*typedefs*/
		using LayerType = Layer<PrevSize, first_int_v<RestSizes...>>;

		template <int LayerIndex>
		using LayerTypeAt = std::conditional_t<
			LayerIndex == Index,
			LayerType,
			typename Base::template LayerTypeAt<LayerIndex>>;

		static constexpr int Size = sizeof...(RestSizes);
		static constexpr bool IsDynamic = false;

	public: /*methods*/
		template<int LayerIndex>
		auto& get();

		template<int LayerIndex>
		const auto& get() const;

	private: /*members*/
		LayerType m_layer;

	};

	/* SPECIALIZED FOR LAST SEQUENCE */

	template<int Index, int PrevSize, int LSize>
	class LayerList<Index, PrevSize, LSize>
	{
	public: /*typedefs*/
		using LayerType = Layer<PrevSize, LSize>;

		template <int LayerIndex>
		using LayerTypeAt = std::conditional_t<
			LayerIndex == Index,
			LayerType,
			void>;

		static constexpr int Size = 1;
		static constexpr bool IsDynamic = false;

	public: /*methods*/
		template<int LayerIndex>
		auto& get();

		template<int LayerIndex>
		const auto& get() const;

	private: /*members*/
		LayerType m_layer;

	};

	template<int ... N>
	using FixedLayers = LayerList<0, N...>;

	/************************************************/
	/**************** IMPLEMENTATION ****************/
	/************************************************/

	template <int Index, int PrevSize, int... RestSizes>
	template <int LayerIndex>
	auto& LayerList<Index, PrevSize, RestSizes...>::get()
	{
		if constexpr (Index == LayerIndex) return m_layer;
		else                               return Base::template get<LayerIndex>();
	}

	template <int Index, int PrevSize, int... RestSizes>
	template <int LayerIndex>
	const auto& LayerList<Index, PrevSize, RestSizes...>::get() const
	{
		if constexpr (Index == LayerIndex) return m_layer;
		else                               return Base::template get<LayerIndex>();
	}

	template <int Index, int PrevSize, int LSize>
	template <int LayerIndex>
	auto& LayerList<Index, PrevSize, LSize>::get()
	{
		static auto null = nullptr;

		if constexpr (Index == LayerIndex) return m_layer;
		else                               return null;
	}

	template <int Index, int PrevSize, int LSize>
	template <int LayerIndex>
	const auto& LayerList<Index, PrevSize, LSize>::get() const
	{
		static auto null = nullptr;

		if constexpr (Index == LayerIndex) return m_layer;
		else                               return null;
	}
}
