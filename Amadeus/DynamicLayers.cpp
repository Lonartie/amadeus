// ******************************************************************************************
// Project: Amadeus
// File:    DynamicLayers.cpp
// Author:  [ ] .
// Created: 2020-07-12
// Changed: 2020-08-21
// ******************************************************************************************

#include "DynamicLayers.h"
using namespace Amadeus;
using namespace AI;

LayerList<Dynamic, Dynamic>::LayerList(const uint64_t size)
{
	m_layers.resize(size);
}

std::vector<DynamicLayer>& LayerList<Dynamic, Dynamic>::layers()
{
	return m_layers;
}

const std::vector<DynamicLayer>& LayerList<Dynamic, Dynamic>::layers() const
{
	return m_layers;
}

uint64_t LayerList<Dynamic, Dynamic>::size() const
{
	return m_layers.size();
}

void LayerList<Dynamic, Dynamic>::addLayer(DynamicLayer&& layer)
{
	m_layers.emplace_back(std::move(layer));
}

void LayerList<Dynamic, Dynamic>::addLayer(DynamicLayer&& layer, const uint64_t index)
{
	AD_ASSERT_IN_CONTAINER_RANGE(m_layers, index);
	
	m_layers.emplace(m_layers.begin() + index, std::move(layer));
}
