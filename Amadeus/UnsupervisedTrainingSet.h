// ******************************************************************************************
// Project: Amadeus
// File:    UnsupervisedTrainingSet.h
// Author:  [ ] .
// Created: 2020-07-31
// Changed: 2020-08-21
// ******************************************************************************************

#pragma once
#include "UnsupervisedTrainingSample.h"

namespace Amadeus::AI
{
	struct UnsupervisedTrainingSet
	{
		std::vector<UnsupervisedTrainingSample> Samples;
	};
}