// ******************************************************************************************
// Project: Amadeus
// File:    DynamicLayer.h
// Author:  [ ] .
// Created: 2020-07-12
// Changed: 2020-08-21
// ******************************************************************************************

#pragma once
#include "Export.h"
#include "CommonMacros.h"
#include "Constants.h"
#include "FixedLayer.h"

namespace Amadeus::AI
{
	template<>
	class AD_EXPORT Layer<Dynamic, Dynamic>
	{
	public: /*typedefs*/
		using InVectorType = Eigen::VectorXf;
		using OutVectorType = Eigen::VectorXf;

		using WeightsType = Eigen::MatrixXf;
		using BiasType = Eigen::VectorXf;

		static constexpr bool IsDynamic = true;

	public: /*ctors*/
		AD_DEFAULT_CDTORS(Layer);
		Layer(uint64_t prevSize, uint64_t size);
		Layer(uint64_t prevSize, uint64_t size, float constant);

	public: /*methods*/
		void setFunc(std::function<float(float)>&& func) { m_func = std::move(func); }
		inline OutVectorType run(const InVectorType& inputs) const;
		
		void initConstant(float constant);
		void initRandom();
		
		void resize(uint64_t prevSize, uint64_t size);
		uint64_t outputs() const;
		uint64_t inputs() const;

	private: /*members*/
		std::function<float(float)> m_func = nullptr;
		WeightsType m_weights;
		BiasType m_bias;
		uint64_t m_prevSize = 0;
		uint64_t m_size = 0;
	};

	using DynamicLayer = Layer<Dynamic, Dynamic>;

	/************************************************/
	/**************** IMPLEMENTATION ****************/
	/************************************************/

	inline Layer<Dynamic, Dynamic>::OutVectorType
		Layer<Dynamic, Dynamic>::run(const InVectorType& inputs) const
	{
		if (m_func)
			return (m_weights * inputs + m_bias).unaryExpr(m_func);
		return m_weights * inputs + m_bias;
	}
}