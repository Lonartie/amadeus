// ******************************************************************************************
// Project: Amadeus
// File:    DynamicPerceptron.h
// Author:  [ ] .
// Created: 2020-07-12
// Changed: 2020-08-21
// ******************************************************************************************

#pragma once
#include "Export.h"
#include "Constants.h"
#include "FixedPerceptron.h"
#include "DynamicANN.h"

namespace Amadeus::Networks
{
	template<>
	class AD_EXPORT Perceptron<Dynamic, Dynamic> : public AI::DynamicANN
	{
	public: /*ctors*/
		Perceptron() = default;
		Perceptron(uint64_t inputs, uint64_t outputs);

	public: /*methods*/
		void resize(uint64_t inputs, uint64_t outputs);

		uint64_t inputs() const;
		uint64_t outputs() const;
		
	private: /*members*/
		AI::Sigmoid m_sig;
		uint64_t m_inputs = 0;
		uint64_t m_outputs = 0;
		
	};

	using DynamicPerceptron = FixedPerceptron<Dynamic, Dynamic>;
}
