// ******************************************************************************************
// Project: Amadeus
// File:    DynamicSTrainer.h
// Author:  [ ] .
// Created: 2020-07-12
// Changed: 2020-08-21
// ******************************************************************************************

#pragma once
#include "Export.h"
#include "Constants.h"
#include "DynamicANN.h"
#include "DynamicTrainingSet.h"
#include "FixedSTrainer.h"

namespace Amadeus::Network::Trainer
{
	template<>
	class AD_EXPORT STrainer<Dynamic>
	{
	public: /*typedefs*/
		using NetworkType = AI::DynamicANN;
		using TrainingSetType = AI::DynamicTrainingSet;

	public: /*ctors*/
		STrainer(NetworkType& network);
		virtual ~STrainer() = default;

	public: /*pure virtual methods*/
		virtual void train(const TrainingSetType& set) = 0;

	public: /*methods*/
		NetworkType& network();
		const NetworkType& network() const;

	private: /*members*/
		NetworkType& m_network;
		
	};

	using DynamicSTrainer = STrainer<Dynamic>;
}