// ******************************************************************************************
// Project: Amadeus
// File:    GradientMap.h
// Author:  [ ] .
// Created: 2020-07-09
// Changed: 2020-08-21
// ******************************************************************************************

#pragma once
#include <Dense>
#include "FixedLayers.h"

namespace Amadeus::AI
{
	template<int ... N>
	struct GradientMap
	{
		using DataType = typename collection<N...>::template to_matrix_tuple<float>;
		DataType Data;
	};
}