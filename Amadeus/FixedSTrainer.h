// ******************************************************************************************
// Project: Amadeus
// File:    FixedSTrainer.h
// Author:  [ ] .
// Created: 2020-07-12
// Changed: 2020-08-21
// ******************************************************************************************

#pragma once
#include "BackPropagator.h"
#include "DynamicOptimizer.h"
#include "Traits.h"
#include "FixedANN.h"
#include "FixedTrainingSet.h"
#include "LossFunction.h"

namespace Amadeus::Network::Trainer
{
	template<int ... N>
	class STrainer
	{
	public: /*typedefs*/
		using NetworkType = AI::FixedANN<N...>;
		using TrainingSetType = AI::FixedTrainingSet<collection<N...>::first, collection<N...>::last>;
		using TrainingSampleType = AI::FixedTrainingSample<collection<N...>::first, collection<N...>::last>;
		using OptimizerType = typename AI::Optimizer<N...>::UPtr;
		using LossFunctionType = AI::LossFunction::SPtr;
		using GradientMapType = typename Algorithms::BackPropagator<N...>::GradientMapType;
		
	public: /*ctors*/
		STrainer(NetworkType& network);
		virtual ~STrainer() = default;

		void setOptimizer(OptimizerType optimizer);
		const OptimizerType& optimizer() const;
		OptimizerType& optimizer();

		void setLossFunction(LossFunctionType lossFunction);
		const LossFunctionType& lossFunction() const;
		LossFunctionType& lossFunction();

	public: /*pure virtual methods*/
		virtual void train(const TrainingSetType& set) = 0;

	protected: /*methods*/
		
	public: /*methods*/
		GradientMapType calculateErrorMap(const TrainingSampleType& sample) const;
		NetworkType& network();
		const NetworkType& network() const;
		
	private:
		NetworkType& m_network;
		OptimizerType m_optimizer;
		LossFunctionType m_lossFunction;
		Algorithms::BackPropagator<N...> m_backPropagator;
	};

	template<int ... N>
	using FixedSTrainer = STrainer<N...>;

	/************************************************/
	/**************** IMPLEMENTATION ****************/
	/************************************************/

	template <int... N>
	STrainer<N...>::STrainer(NetworkType& network)
		: m_network(network)
	{}

	template <int... N>
	void STrainer<N...>::setOptimizer(OptimizerType optimizer)
	{
		m_optimizer = std::move(optimizer);
	}

	template <int... N>
	const typename STrainer<N...>::OptimizerType& STrainer<N...>::optimizer() const
	{
		return m_optimizer;
	}

	template <int... N>
	typename STrainer<N...>::OptimizerType& STrainer<N...>::optimizer()
	{
		return m_optimizer;
	}

	template <int... N>
	void STrainer<N...>::setLossFunction(LossFunctionType lossFunction)
	{
		m_lossFunction = std::move(lossFunction);
		m_backPropagator.setLossFunction(m_lossFunction);
	}

	template <int... N>
	const typename STrainer<N...>::LossFunctionType& STrainer<N...>::lossFunction() const
	{
		return m_lossFunction;
	}

	template <int... N>
	typename STrainer<N...>::LossFunctionType& STrainer<N...>::lossFunction()
	{
		return m_lossFunction;
	}

	template <int... N>
	typename STrainer<N...>::GradientMapType STrainer<N...>::calculateErrorMap(const TrainingSampleType& sample) const
	{
		return m_backPropagator.calculateErrorMap(m_network, sample);
	}

	template <int... N>
	typename STrainer<N...>::NetworkType& STrainer<N...>::network()
	{
		return m_network;
	}

	template <int... N>
	const typename STrainer<N...>::NetworkType& STrainer<N...>::network() const
	{
		return m_network;
	}
}
