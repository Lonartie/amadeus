// ******************************************************************************************
// Project: Amadeus
// File:    DynamicANN.h
// Author:  [ ] .
// Created: 2020-07-12
// Changed: 2020-08-21
// ******************************************************************************************

#pragma once
#include "Export.h"
#include "CommonMacros.h"
#include "Constants.h"
#include "FixedANN.h"
#include "DynamicLayers.h"
#include <Dense>

namespace Amadeus::AI
{
	template<>
	class AD_EXPORT ANN<Dynamic>
	{
	public: /*typedefs*/
		AD_MEMORY(ANN<Dynamic>);
		
		using LayersType = DynamicLayers;

		using InVectorType = Eigen::VectorXf;
		using OutVectorType = Eigen::VectorXf;

		static constexpr bool IsDynamic = true;

	public: /*ctors*/
		AD_DEFAULT_CDTORS(ANN);
		ANN(uint64_t size);

	public: /*methods*/
		uint64_t size() const;

		inline OutVectorType run(const InVectorType& inputs) const;

		DynamicLayer& layer(uint64_t index);
		const DynamicLayer& layer(uint64_t index) const;

		void addLayer(DynamicLayer&& layer);
		void addLayer(DynamicLayer&& layer, uint64_t index);

		std::vector<DynamicLayer>& layers();
		const std::vector<DynamicLayer>& layers() const;

	private: /*methods*/
		inline Eigen::VectorXf runLayer(uint64_t index, const InVectorType& inputs) const;

	private: /*members*/
		DynamicLayers m_layers;

	};

	using DynamicANN = ANN<Dynamic>;

	/************************************************/
	/**************** IMPLEMENTATION ****************/
	/************************************************/

	inline ANN<Dynamic>::OutVectorType
		ANN<Dynamic>::run(const InVectorType& inputs) const
	{
		return runLayer(m_layers.size() - 1, inputs);
	}

	Eigen::VectorXf ANN<Dynamic>::runLayer(const uint64_t index, const InVectorType& inputs) const
	{
		if (index == 0)
			return m_layers.get(index).run(inputs);
		return runLayer(index - 1, m_layers.get(index).run(inputs));
	}
}