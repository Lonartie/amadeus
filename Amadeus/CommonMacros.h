// ******************************************************************************************
// Project: Amadeus
// File:    CommonMacros.h
// Author:  [ ] .
// Created: 2020-07-12
// Changed: 2020-08-21
// ******************************************************************************************

#pragma once
#include <memory>

#define AD_MEMORY(CLS) \
using SPtr = std::shared_ptr<CLS>; \
using UPtr = std::unique_ptr<CLS>

#define AD_DEFAULT_CTOR(CLS) \
CLS() = default;

#define AD_DEFAULT_COPY_CTOR(CLS) \
CLS(const CLS&) = default; \
CLS& operator=(const CLS&) = default;

#define AD_DEFAULT_MOVE_CTOR(CLS) \
CLS(CLS&&) noexcept = default; \
CLS& operator=(CLS&&) noexcept = default;

#define AD_DEFAULT_DTOR(CLS) \
virtual ~CLS() = default;

#define AD_DEFAULT_CDTORS(CLS) \
AD_DEFAULT_CTOR(CLS) \
AD_DEFAULT_COPY_CTOR(CLS) \
AD_DEFAULT_MOVE_CTOR(CLS) \
AD_DEFAULT_DTOR(CLS)


