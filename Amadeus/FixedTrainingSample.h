// ******************************************************************************************
// Project: Amadeus
// File:    FixedTrainingSample.h
// Author:  [ ] .
// Created: 2020-07-07
// Changed: 2020-08-21
// ******************************************************************************************

#pragma once
#include <Dense>

namespace Amadeus::AI
{
	template<int Inputs, int Outputs>
	struct TrainingSample final
	{
		Eigen::Matrix<float, Inputs, 1> Input;
		Eigen::Matrix<float, Outputs, 1> Target;
	};

	template<int Inputs, int Outputs>
	using FixedTrainingSample = TrainingSample<Inputs, Outputs>;
}