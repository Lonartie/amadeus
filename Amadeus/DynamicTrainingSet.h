// ******************************************************************************************
// Project: Amadeus
// File:    DynamicTrainingSet.h
// Author:  [ ] .
// Created: 2020-07-12
// Changed: 2020-08-21
// ******************************************************************************************

#pragma once
#include "Constants.h"
#include "DynamicTrainingSample.h"
#include "FixedTrainingSet.h"

namespace Amadeus::AI
{
	template<>
	struct TrainingSet<Dynamic, Dynamic> final
	{
		std::vector<DynamicTrainingSample> Samples;		
	};

	using DynamicTrainingSet = TrainingSet<Dynamic, Dynamic>;
}