// ******************************************************************************************
// Project: Amadeus
// File:    DynamicLayers.h
// Author:  [ ] .
// Created: 2020-07-12
// Changed: 2020-08-21
// ******************************************************************************************

#pragma once
#include "Export.h"
#include "CommonMacros.h"
#include "Constants.h"
#include "FixedLayers.h"
#include "DynamicLayer.h"
#include "DebugMacros.h"
#include <Dense>

namespace Amadeus::AI
{
	template<>
	class AD_EXPORT LayerList<Dynamic, Dynamic>
	{
	public: /*typedefs*/
		using LayerType = DynamicLayer;

		static constexpr int Size = Dynamic;
		static constexpr bool IsDynamic = true;

	public: /*ctors*/
		AD_DEFAULT_CDTORS(LayerList);
		LayerList(uint64_t size);

	public: /*methods*/
		inline DynamicLayer& get(uint64_t index);
		inline const DynamicLayer& get(uint64_t index) const;

		std::vector<DynamicLayer>& layers();
		const std::vector<DynamicLayer>& layers() const;

		uint64_t size() const;

		void addLayer(DynamicLayer&& layer);
		void addLayer(DynamicLayer&& layer, uint64_t index);

	private: /*members*/
		std::vector<DynamicLayer> m_layers;

	};
	
	using DynamicLayers = LayerList<Dynamic, Dynamic>;

	/************************************************/
	/**************** IMPLEMENTATION ****************/
	/************************************************/

	inline DynamicLayer& LayerList<Dynamic, Dynamic>::get(const uint64_t index)
	{
		AD_ASSERT_IN_CONTAINER_RANGE(m_layers, index);

		return m_layers[index];
	}

	inline const DynamicLayer& LayerList<Dynamic, Dynamic>::get(const uint64_t index) const
	{
		AD_ASSERT_IN_CONTAINER_RANGE(m_layers, index);

		return m_layers[index];
	}
}
