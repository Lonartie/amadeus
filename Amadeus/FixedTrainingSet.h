// ******************************************************************************************
// Project: Amadeus
// File:    FixedTrainingSet.h
// Author:  [ ] .
// Created: 2020-07-07
// Changed: 2020-08-21
// ******************************************************************************************

#pragma once
#include "FixedTrainingSample.h"
#include <vector>

namespace Amadeus::AI
{
	template<int Inputs, int Outputs>
	struct TrainingSet final
	{
		std::vector<FixedTrainingSample<Inputs, Outputs>> Samples;
	};

	template<int Inputs, int Outputs>
	using FixedTrainingSet = TrainingSet<Inputs, Outputs>;
	
}