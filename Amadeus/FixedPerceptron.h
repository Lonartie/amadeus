// ******************************************************************************************
// Project: Amadeus
// File:    FixedPerceptron.h
// Author:  [ ] .
// Created: 2020-07-12
// Changed: 2020-08-21
// ******************************************************************************************

#pragma once
#include "FixedANN.h"
#include "Sigmoid.h"

namespace Amadeus::Networks
{
	template<int Inputs, int Outputs>
	class Perceptron final: public AI::FixedANN<Inputs, 1, Outputs>
	{
		using Base = AI::ANN<Inputs, 1, Outputs>;
		
	public: /*ctors*/
		Perceptron();

	private: /*members*/
		AI::Sigmoid m_sig;

	};

	template<int Inputs, int Outputs>
	using FixedPerceptron = Perceptron<Inputs, Outputs>;

	/************************************************/
	/**************** IMPLEMENTATION ****************/
	/************************************************/

	template <int Inputs, int Outputs>
	Perceptron<Inputs, Outputs>::Perceptron()
	{
		// setting hidden layer activation function to sigmoid
		Base::template layer<0>().setFunc([this](float x) {return m_sig.run(x); });
		Base::template layer<0>().setDerivative([this](float x) {return m_sig.derivative(x); });
		Base::template layer<0>().initRandom();

		// setting output layer activation function to sigmoid
		Base::template layer<1>().setFunc([this](float x) {return m_sig.run(x); });
		Base::template layer<1>().setDerivative([this](float x) {return m_sig.derivative(x); });
		Base::template layer<1>().initRandom();
	}
}
