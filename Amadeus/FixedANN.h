// ******************************************************************************************
// Project: Amadeus
// File:    FixedANN.h
// Author:  [ ] .
// Created: 2020-07-12
// Changed: 2020-08-21
// ******************************************************************************************

#pragma once
#include "CommonMacros.h"
#include "FixedLayers.h"

namespace Amadeus::AI
{
	template<int InputSize, int ... RestSizes>
	class ANN
	{
	public: /*typedefs*/
		AD_MEMORY(ANN);
		
		using LayersType = FixedLayers<InputSize, RestSizes...>;

		using InVectorType = typename LayersType::template LayerTypeAt<0>::InVectorType;
		using OutVectorType = typename LayersType::template LayerTypeAt<sizeof...(RestSizes) - 1>::OutVectorType;

		static constexpr int Size = sizeof...(RestSizes);
		static constexpr bool IsDynamic = false;

	public: /*ctors*/
		virtual ~ANN() = default;

	public: /*normal methods*/
		OutVectorType run(const InVectorType& inputs) const;

	public: /*templated methods*/
		template <int Index>
		auto& layer();

		template <int Index>
		const auto& layer() const;

	private: /*methods*/
		template <int Index>
		auto runLayer(const InVectorType& inputs) const;

	private: /*members*/
		LayersType m_layers;
	};

	template<int ... N>
	using FixedANN = ANN<N...>;

	/************************************************/
	/**************** IMPLEMENTATION ****************/
	/************************************************/

	template <int InputSize, int... RestSizes>
	typename ANN<InputSize, RestSizes...>::OutVectorType
		ANN<InputSize, RestSizes...>::run(const InVectorType& inputs) const
	{
		return runLayer<0>(inputs);
	}

	template <int InputSize, int... RestSizes>
	template <int Index>
	auto& ANN<InputSize, RestSizes...>::layer()
	{
		return m_layers.template get<Index>();
	}

	template <int InputSize, int... RestSizes>
	template <int Index>
	const auto& ANN<InputSize, RestSizes...>::layer() const
	{
		return m_layers.template get<Index>();
	}

	template <int InputSize, int... RestSizes>
	template <int Index>
	auto ANN<InputSize, RestSizes...>::runLayer(const InVectorType& inputs) const
	{
		if constexpr (Index < LayersType::Size - 1)
			return layer<LayersType::Size - Index - 1>().run(runLayer<Index + 1>(inputs));
		else
			return layer<LayersType::Size - Index - 1>().run(inputs);
	}
}
