// ******************************************************************************************
// Project: Amadeus
// File:    BackPropagator.h
// Author:  [ ] .
// Created: 2020-08-07
// Changed: 2020-08-21
// ******************************************************************************************

#pragma once
#include "FixedANN.h"
#include "FixedTrainingSample.h"
#include "LossFunction.h"
#include <Dense>
#include <array>

namespace Amadeus::Network::Algorithms
{

	template<int ... N>
	class BackPropagator
	{
	public: /*typedefs*/
		using NetworkType = AI::FixedANN<N...>;
		using TrainingSampleType = AI::FixedTrainingSample<collection<N...>::first, collection<N...>::last>;
		using GradientMapType = AI::GradientMap<N...>;
		using LossFunctionType = AI::LossFunction::SPtr;

		template<int Index>
		using LayerVectorType = Eigen::Matrix<float, nth_element_v<Index + 1, N...>, 1>;

		template<int Index>
		using LayerMatrixType = decltype(std::get<Index>(std::declval<typename GradientMapType::DataType>()));

	public: /*properties*/
		const LossFunctionType& lossFunction() const;
		void setLossFunction(const LossFunctionType& lossFunction);

	public: /*logic*/
		GradientMapType calculateErrorMap(const NetworkType& network,
													 const TrainingSampleType& sample) const;

	private:

		GradientMapType calculateRecursivelyBegin(const NetworkType& network,
																const LayerVectorType<sizeof...(N) - 2>& output,
																const LayerVectorType<sizeof...(N) - 2>& target) const;

		template<int LayerIndex>
		GradientMapType calculateRecursively(const NetworkType& network,
														 const LayerVectorType<LayerIndex + 1>& output,
														 const LayerVectorType<LayerIndex + 1>& target) const;

	private: /*members*/
		LossFunctionType m_lossFunction;
	};

	/************************************************/
	/**************** IMPLEMENTATION ****************/
	/************************************************/

	template <int... N>
	const typename BackPropagator<N...>::LossFunctionType& BackPropagator<N...>::lossFunction() const
	{
		return m_lossFunction;
	}

	template <int... N>
	void BackPropagator<N...>::setLossFunction(const LossFunctionType& lossFunction)
	{
		m_lossFunction = lossFunction;
	}

	template <int... N>
	typename BackPropagator<N...>::GradientMapType BackPropagator<N...>::calculateErrorMap(const NetworkType& network, const TrainingSampleType& sample) const
	{
		return this->calculateRecursivelyBegin(network, network.run(sample.Input), sample.Target);
	}

	template <int... N>
	typename BackPropagator<N...>::GradientMapType
		BackPropagator<N...>::calculateRecursivelyBegin(const NetworkType& network,
																		const LayerVectorType<sizeof...(N) - 2>& output,
																		const LayerVectorType<sizeof...(N) - 2>& target) const
	{
		constexpr int LayerIndex = sizeof...(N) - 2;
		using Vector = LayerVectorType<LayerIndex>;
		using Matrix = LayerMatrixType<LayerIndex>;

		constexpr int currentSize = nth_element_v<LayerIndex, N...>;
		const auto& currentLayer = network.template layer<LayerIndex>();
		const auto& previousLayer = network.template layer<LayerIndex - 1>();

		// D(ErrorTotal) / D(output) -> loss function'
		Vector de_do;
		for (int i = 0; i < currentSize; ++i)
		{
			de_do[i] = m_lossFunction->derivative(target[i], output[i]);
		}

		// D(output) / D(input) -> activation function'
		const Vector do_di = currentLayer.derivative(currentLayer.lastInputs());

		// D(input) / D(w) -> output
		const Vector di_dw = currentLayer.lastOutputs();

		auto errors = this->template calculateRecursively<LayerIndex - 1>(network, previousLayer.lastOutputs(), de_do);
		std::get<LayerIndex>(errors.Data) = de_do.cwiseProduct(do_di).cwiseProduct(di_dw);
		return errors;
	}

	template <int... N>
	template <int LayerIndex>
	typename BackPropagator<N...>::GradientMapType
		BackPropagator<N...>::calculateRecursively(const NetworkType& network,
																 const LayerVectorType<LayerIndex + 1>& output,
																 const LayerVectorType<LayerIndex + 1>& target) const
	{
		using Vector = LayerVectorType<LayerIndex>;
		using Matrix = LayerMatrixType<LayerIndex>;
		constexpr int nextSize = nth_element_v<LayerIndex + 2, N...>;
		constexpr int currentSize = nth_element_v<LayerIndex + 1, N...>;
		const auto& nextLayer = network.template layer<LayerIndex + 1>();
		const auto& currentLayer = network.template layer<LayerIndex>();

		const auto& nWeight = nextLayer.weights();
		const auto& cWeights = currentLayer.weights();
		const auto& cOutput = currentLayer.lastOutputs();


		// For each weight -> calculate gradient
		Matrix gradients;
		for (auto r = 0; r < cWeights.rows(); ++r)
		for (auto c = 0; c < cWeights.cols(); ++c)
		{
			auto& weight = currentLayer.weights()(r, c);
			// D(ErrorTotal) / D(output) -> partial derivative
			Vector de_do;

			for (auto i = 0; i < currentSize * nextSize; ++i)
			{
				// D(Error[n] / D(output[n]) -> loss function'
				const auto den_don = m_lossFunction->derivative(target[n], output[n]);

				// D(output[n] / D(input[n]) -> activation function'
				const auto don_din = nextLayer.derivative(output[n]);

				// D(input[n] / D(output[n]) -> w
				const auto din_don = nWeight(c, n);

				de_do[c] = den_don * don_din * din_don;
			}

			// D(output) / D(input) -> activation function'
			const Vector do_di = currentLayer.derivative(currentLayer.lastOutputs());

			// D(input) / D(w) -> output
			const Vector di_dw = currentLayer.lastOutputs();

			const Vector gradient = de_do.cwiseProduct(do_di).cwiseProduct(di_dw);

			gradients(r, c) = gradient;
		}

		if constexpr (LayerIndex != 0)
		{
			auto errors = this->template calculateRecursively<LayerIndex - 1>(network, currentLayer.lastOutputs(), de_do);
			std::get<LayerIndex>(errors.Data) = gradients;
			return errors;
		} else
		{
			GradientMapType errors;
			std::get<0>(errors.Data) = gradients;
			return errors;
		}
	}
}
