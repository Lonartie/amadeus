// ******************************************************************************************
// Project: Amadeus
// File:    FixedLayer.h
// Author:  [ ] .
// Created: 2020-07-12
// Changed: 2020-08-21
// ******************************************************************************************

#pragma once
#include <Dense>

namespace Amadeus::AI
{
	template<int PrevSize, int Size>
	class Layer final
	{
	public: /*typedefs*/
		using InVectorType = Eigen::Matrix<float, PrevSize, 1>;
		using OutVectorType = Eigen::Matrix<float, Size, 1>;

		using WeightsType = Eigen::Matrix<float, Size, PrevSize>;
		using BiasType = Eigen::Matrix<float, Size, 1>;

		static constexpr bool IsDynamic = false;

	public: /*ctors*/
		Layer() = default;
		Layer(float constant);
		
	public: /*methods*/
		void setFunc(std::function<float(float)>&& func) { m_func = std::move(func); }
		void setDerivative(std::function<float(float)>&& derivative) { m_deri = std::move(derivative); }
		
		OutVectorType run(const InVectorType& inputs) const;
		OutVectorType derivative(const OutVectorType& inputs) const;
		float derivative(float input) const;
		
		void initConstant(float constant);
		void initRandom();

		const InVectorType& lastInputs() const;
		const OutVectorType& lastOutputs() const;

		const WeightsType& weights() const;
		WeightsType& weights();

		const BiasType& bias() const;
		BiasType& bias();
		
		static uint64_t outputs();
		static uint64_t inputs();

	private: /*members*/
		std::function<float(float)> m_func = nullptr;
		std::function<float(float)> m_deri = nullptr;
		WeightsType m_weights;
		BiasType m_bias;
		mutable InVectorType m_lastInputs;
		mutable OutVectorType m_lastOutputs;

	};

	template<int PreviousSize, int Size>
	using FixedLayer = Layer<PreviousSize, Size>;

	/************************************************/
	/**************** IMPLEMENTATION ****************/
	/************************************************/

	template <int PrevSize, int Size>
	Layer<PrevSize, Size>::Layer(const float constant)
	{
		initConstant(constant);
	}

	template <int PrevSize, int Size>
	typename Layer<PrevSize, Size>::OutVectorType Layer<PrevSize, Size>::run(const InVectorType& inputs) const
	{
		m_lastInputs = InVectorType(inputs);

		if (m_func) m_lastOutputs = (m_weights * inputs + m_bias).unaryExpr(m_func);
		else        m_lastOutputs = m_weights * inputs + m_bias;

		return m_lastOutputs;
	}

	template <int PrevSize, int Size>
	typename Layer<PrevSize, Size>::OutVectorType Layer<PrevSize, Size>::derivative(const OutVectorType& inputs) const
	{
		return inputs.unaryExpr(m_deri);
	}

	template <int PrevSize, int Size>
	float Layer<PrevSize, Size>::derivative(const float input) const
	{
		return m_deri(input);
	}

	template <int PrevSize, int Size>
	void Layer<PrevSize, Size>::initConstant(const float constant)
	{
		m_weights = WeightsType::Constant(constant);
		m_bias = BiasType::Constant(constant);
	}

	template <int PrevSize, int Size>
	void Layer<PrevSize, Size>::initRandom()
	{
		m_weights = WeightsType::Random().unaryExpr([](const float x) { return x * 4 - 2; });
		m_bias = BiasType::Random().unaryExpr([](const float x) { return x * 4 - 2; });
	}

	template <int PrevSize, int Size>
	const typename Layer<PrevSize, Size>::InVectorType& Layer<PrevSize, Size>::lastInputs() const
	{
		return m_lastInputs;
	}

	template <int PrevSize, int Size>
	const typename Layer<PrevSize, Size>::OutVectorType& Layer<PrevSize, Size>::lastOutputs() const
	{
		return m_lastOutputs;
	}

	template <int PrevSize, int Size>
	const typename Layer<PrevSize, Size>::WeightsType& Layer<PrevSize, Size>::weights() const
	{
		return m_weights;
	}

	template <int PrevSize, int Size>
	typename Layer<PrevSize, Size>::WeightsType& Layer<PrevSize, Size>::weights()
	{
		return m_weights;
	}

	template <int PrevSize, int Size>
	const typename Layer<PrevSize, Size>::BiasType& Layer<PrevSize, Size>::bias() const
	{
		return m_bias;
	}

	template <int PrevSize, int Size>
	typename Layer<PrevSize, Size>::BiasType& Layer<PrevSize, Size>::bias()
	{
		return m_bias;
	}

	template <int PrevSize, int Size>
	uint64_t Layer<PrevSize, Size>::outputs()
	{
		return Size;
	}

	template <int PrevSize, int Size>
	uint64_t Layer<PrevSize, Size>::inputs()
	{
		return PrevSize;
	}
}
