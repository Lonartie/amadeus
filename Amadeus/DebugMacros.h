// ******************************************************************************************
// Project: Amadeus
// File:    DebugMacros.h
// Author:  [ ] .
// Created: 2020-07-05
// Changed: 2020-08-21
// ******************************************************************************************

#pragma once
#include <QtCore/QString>

template<typename L, typename ...R>
QString qstrfmt(const QString& base, L left, R...right)
{
	return qstrfmt(base.arg(left), right...);
}

template<typename L>
QString qstrfmt(const QString& base, L left)
{
	return base.arg(left);
}

#define AD_FUNC_NAME __FUNCSIG__
#define _AD_EXPAND(...) __VA_ARGS__
#define AD_EXPAND(...) _AD_EXPAND(__VA_ARGS__)
#define AD_STRING(X) ( QString::fromStdString((std::stringstream() << X).str()) )
#define _AD_STRINGIFY(X) #X
#define AD_STRINGIFY(X) _AD_STRINGIFY(X)
#define AD_SC(BASE, ...) ( qstrfmt ( BASE , __VA_ARGS__ ) )
#define AD_THROW(MSG) { throw std::runtime_error(AD_SC("Assertion failed in '%1': %2", AD_FUNC_NAME, MSG ).toStdString()); }

#if defined(NDEBUG)
#	define AD_DEBUG_ONLY(...)
#else
#	define AD_DEBUG_ONLY(...) AD_EXPAND(__VA_ARGS__)
#endif

#define AD_ASSERT(COND, MSG) AD_DEBUG_ONLY({ if (!(COND)) AD_THROW(MSG) })
#define AD_ASSERT_EQ(A, B) AD_DEBUG_ONLY({ if((A) != (B)) AD_THROW(AD_SC("%1 != %2 [%3 != %4]", AD_STRINGIFY(A), AD_STRINGIFY(B), AD_STRING(A), AD_STRING(B))) })
#define AD_ASSERT_NEQ(A, B) AD_DEBUG_ONLY({ if((A) == (B)) AD_THROW(AD_SC("%1 == %2 [%3 == %4]", AD_STRINGIFY(A), AD_STRINGIFY(B), AD_STRING(A), AD_STRING(B))) })
#define AD_ASSERT_NOT_EMPTY(CONTAINER) AD_DEBUG_ONLY({ if (std::begin(CONTAINER) == std::end(CONTAINER)) AD_THROW("Container may not be empty!") })
#define AD_ASSERT_IN_CONTAINER_RANGE(CONTAINER, VALUE) AD_ASSERT((VALUE) >= 0 && (VALUE) < (CONTAINER).size(), AD_SC("%1 is not in range of %2 (%3 -> [0 .. %4])", AD_STRINGIFY(VALUE), AD_STRINGIFY(CONTAINER), AD_STRING(VALUE), AD_STRING((CONTAINER).size()) ))
#define AD_ASSERT_SAME_SIZE(VEC_A, VEC_B) AD_ASSERT((VEC_A).size() == (VEC_B).size(), AD_SC("Vectors must have same size (%1 [%2] != %3 [%4])", AD_STRINGIFY(VEC_A), (VEC_A).size(), AD_STRINGIFY(VEC_B), (VEC_B).size()))

#define AD_ASSERT_MATRIX_SIZE_CONSISTENT(MATRIX) AD_DEBUG_ONLY({ auto AD_MATRIX_SIZE = (MATRIX)[0].size(); for (uint64_t i = 1; i < (MATRIX).size(); ++i) AD_ASSERT((MATRIX)[i].size() == AD_MATRIX_SIZE, AD_SC("matrix must have consistent size!\n%1", AD_STRING(MATRIX))); })
