// ******************************************************************************************
// Project: Amadeus
// File:    DynamicANN.cpp
// Author:  [ ] .
// Created: 2020-07-12
// Changed: 2020-08-21
// ******************************************************************************************

#include "DynamicANN.h"
using namespace Amadeus;
using namespace AI;

ANN<Dynamic>::ANN(const uint64_t size)
	: m_layers(size)
{}

uint64_t ANN<Dynamic>::size() const
{
	return m_layers.size();
}

DynamicLayer& ANN<Dynamic>::layer(const uint64_t index)
{
	return m_layers.get(index);
}

const DynamicLayer& ANN<Dynamic>::layer(const uint64_t index) const
{
	return m_layers.get(index);
}

void ANN<Dynamic>::addLayer(DynamicLayer&& layer)
{
	m_layers.addLayer(std::move(layer));
}

void ANN<Dynamic>::addLayer(DynamicLayer&& layer, const uint64_t index)
{
	m_layers.addLayer(std::move(layer), index);	
}

std::vector<DynamicLayer>& ANN<Dynamic>::layers()
{
	return m_layers.layers();
}

const std::vector<DynamicLayer>& ANN<Dynamic>::layers() const
{
	return m_layers.layers();
}
