// ******************************************************************************************
// Project: Amadeus.Tests
// File:    DynamicLayerTests.cpp
// Author:  [ ] .
// Created: 2020-07-12
// Changed: 2020-08-21
// ******************************************************************************************

#include <boost/test/unit_test.hpp>
#include "Amadeus/Amadeus"
using namespace Amadeus::AI;

BOOST_AUTO_TEST_SUITE(TS_Layer);
BOOST_AUTO_TEST_SUITE(TS_DynamicLayer);
BOOST_AUTO_TEST_SUITE(Structure);

BOOST_AUTO_TEST_CASE(DynamicLayerIsDynamic)
{
	static_assert(DynamicLayer::IsDynamic, "DynamicLayer must be dynamic!");
	BOOST_CHECK(true);
}

BOOST_AUTO_TEST_CASE(CanBeResized)
{
	DynamicLayer layer(2, 2, 1);
	const auto result = layer.run(Eigen::Vector2f::Constant(2));
	BOOST_TEST(result.size() == 2);
	layer = DynamicLayer(1, 3, 1);
	const auto result2 = layer.run(Eigen::Matrix<float, 1, 1>::Constant(2));
	BOOST_TEST(result2.size() == 3);
	layer.resize(4, 5);
	layer.initConstant(1);
	const auto result3 = layer.run(Eigen::Vector4f::Constant(2));
	BOOST_TEST(result3.size() == 5);
}

BOOST_AUTO_TEST_SUITE_END();
BOOST_AUTO_TEST_SUITE(Calculation);

BOOST_AUTO_TEST_CASE(Calculation_01)
{
	const DynamicLayer layer(3, 4, 3);
	const auto result = layer.run(Eigen::Vector3f::Constant(2));
	const Eigen::Vector4f expected = {21, 21, 21, 21};
	BOOST_TEST(result == expected);
}

BOOST_AUTO_TEST_CASE(Calculation_02)
{
	const DynamicLayer layer(4, 3, 3);
	const auto result = layer.run(Eigen::Vector4f::Constant(2));
	const Eigen::Vector3f expected = {27, 27, 27};
	BOOST_TEST(result == expected);
}

BOOST_AUTO_TEST_CASE(Calculation_03)
{
	const Sigmoid sig;
	DynamicLayer layer(4, 3, 0.5);
	layer.setFunc([&sig](const float x) {return sig.run(x); });
	const auto result = layer.run(Eigen::Vector4f::Constant(0.3));
	const Eigen::Vector3f expected = {0.750260115F, 0.750260115F, 0.750260115F};
	BOOST_TEST(result == expected);
}

BOOST_AUTO_TEST_SUITE_END();
BOOST_AUTO_TEST_SUITE_END();
BOOST_AUTO_TEST_SUITE_END();