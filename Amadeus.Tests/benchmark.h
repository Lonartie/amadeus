// ******************************************************************************************
// Project: Amadeus.Tests
// File:    benchmark.h
// Author:  [ ] .
// Created: 2020-07-05
// Changed: 2020-08-21
// ******************************************************************************************

#pragma once
#include <functional>
#include <chrono>

namespace std::chrono
{
	using picoseconds = duration<long long, pico>;
	using femtoseconds = duration<long long, femto>;
	using attoseconds = duration<long long, atto>;
}

class Benchmark
{
public:
	Benchmark(std::function<void()>&& f);

	void run(uint64_t calls);

	float callsPerSecond() const;
	float millisecondsPerCall() const;
	std::string formattedTimePerCall() const;
	std::string formattedTimeForAllCalls() const;
	std::string formattedMinTimePerCall() const;
	std::string formattedMaxTimePerCall() const;

private:
	std::function<void()> m_f;
	uint64_t m_calls = 0;
	std::chrono::picoseconds m_dur;
	std::chrono::picoseconds m_minDur;
	std::chrono::picoseconds m_maxDur;
};

inline Benchmark::Benchmark(std::function<void()>&& f)
	: m_f(std::move(f))
{}

inline void Benchmark::run(uint64_t calls)
{
	m_calls = calls;
	m_dur = std::chrono::picoseconds(0);
	m_minDur = std::chrono::picoseconds::max();
	m_maxDur = std::chrono::picoseconds::min();
	
	const auto ohcalc_begin = std::chrono::high_resolution_clock::now();
	const auto ohcalc_end = std::chrono::high_resolution_clock::now();
	const auto oh = std::chrono::duration_cast<std::chrono::picoseconds>(ohcalc_end - ohcalc_begin);
	
	while (calls--)
	{
		const auto begin = std::chrono::high_resolution_clock::now();
		m_f();
		const auto end = std::chrono::high_resolution_clock::now();
		const auto dif = std::chrono::duration_cast<std::chrono::picoseconds>(end - begin) - oh;
		m_dur += dif;
		m_minDur = std::min(m_minDur, dif);
		m_maxDur = std::max(m_maxDur, dif);
	}
}

inline float Benchmark::callsPerSecond() const
{
	return m_calls / (m_dur.count() / 1000.0f / 1000.0f / 1000.0f / 1000.0f);
}

inline float Benchmark::millisecondsPerCall() const
{
	return m_dur.count() / static_cast<float>(m_calls);
}

inline std::string Benchmark::formattedTimePerCall() const
{
	const auto hours_per_call = std::chrono::duration_cast<std::chrono::hours>(m_dur).count() / static_cast<float>(m_calls);
	if (hours_per_call >= 1) return std::to_string(hours_per_call) + " h/call";

	const auto minutes_per_call = std::chrono::duration_cast<std::chrono::minutes>(m_dur).count() / static_cast<float>(m_calls);
	if (minutes_per_call >= 1) return std::to_string(minutes_per_call) + " m/call";

	const auto seconds_per_call = std::chrono::duration_cast<std::chrono::seconds>(m_dur).count() / static_cast<float>(m_calls);
	if (seconds_per_call >= 1) return std::to_string(seconds_per_call) + " s/call";

	const auto milliseconds_per_call = std::chrono::duration_cast<std::chrono::milliseconds>(m_dur).count() / static_cast<float>(m_calls);
	if (milliseconds_per_call >= 1) return std::to_string(milliseconds_per_call) + " ms/call";

	const auto microseconds_per_call = std::chrono::duration_cast<std::chrono::microseconds>(m_dur).count() / static_cast<float>(m_calls);
	if (microseconds_per_call >= 1) return std::to_string(microseconds_per_call) + " �s/call";

	const auto nanoseconds_per_call = std::chrono::duration_cast<std::chrono::nanoseconds>(m_dur).count() / static_cast<float>(m_calls);
	/*if (nanoseconds_per_call >= 1)*/
	return std::to_string(nanoseconds_per_call) + " ns/call";
}

inline std::string Benchmark::formattedMinTimePerCall() const
{
	const auto hours_per_call = std::chrono::duration_cast<std::chrono::minutes>(m_minDur).count() / 60.0;
	if (hours_per_call >= 1) return std::to_string(hours_per_call) + " h";

	const auto minutes_per_call = std::chrono::duration_cast<std::chrono::seconds>(m_minDur).count() / 60.0;
	if (minutes_per_call >= 1) return std::to_string(minutes_per_call) + " m";

	const auto seconds_per_call = std::chrono::duration_cast<std::chrono::milliseconds>(m_minDur).count() / 1000.0;
	if (seconds_per_call >= 1) return std::to_string(seconds_per_call) + " s";

	const auto milliseconds_per_call = std::chrono::duration_cast<std::chrono::microseconds>(m_minDur).count() / 1000.0;
	if (milliseconds_per_call >= 1) return std::to_string(milliseconds_per_call) + " ms";

	const auto microseconds_per_call = std::chrono::duration_cast<std::chrono::nanoseconds>(m_minDur).count() / 1000.0;
	if (microseconds_per_call >= 1) return std::to_string(microseconds_per_call) + " �s";

	const auto nanoseconds_per_call = std::chrono::duration_cast<std::chrono::picoseconds>(m_minDur).count() / 1000.0;
	/*if (nanoseconds_per_call >= 1)*/
	return std::to_string(nanoseconds_per_call) + " ns";
}

inline std::string Benchmark::formattedMaxTimePerCall() const
{
	const auto hours_per_call = std::chrono::duration_cast<std::chrono::minutes>(m_maxDur).count() / 60.0;
	if (hours_per_call >= 1) return std::to_string(hours_per_call) + " h";

	const auto minutes_per_call = std::chrono::duration_cast<std::chrono::seconds>(m_maxDur).count() / 60.0;
	if (minutes_per_call >= 1) return std::to_string(minutes_per_call) + " m";

	const auto seconds_per_call = std::chrono::duration_cast<std::chrono::milliseconds>(m_maxDur).count() / 1000.0;
	if (seconds_per_call >= 1) return std::to_string(seconds_per_call) + " s";

	const auto milliseconds_per_call = std::chrono::duration_cast<std::chrono::microseconds>(m_maxDur).count() / 1000.0;
	if (milliseconds_per_call >= 1) return std::to_string(milliseconds_per_call) + " ms";

	const auto microseconds_per_call = std::chrono::duration_cast<std::chrono::nanoseconds>(m_maxDur).count() / 1000.0;
	if (microseconds_per_call >= 1) return std::to_string(microseconds_per_call) + " �s";

	const auto nanoseconds_per_call = std::chrono::duration_cast<std::chrono::picoseconds>(m_maxDur).count() / 1000.0;
	/*if (nanoseconds_per_call >= 1)*/
	return std::to_string(nanoseconds_per_call) + " ns";
}

inline std::string Benchmark::formattedTimeForAllCalls() const
{
	const auto milliseconds_per_call = std::chrono::duration_cast<std::chrono::nanoseconds>(m_dur).count() / 1000.0 / 1000.0;
	return std::to_string(milliseconds_per_call) + " ms";
}
