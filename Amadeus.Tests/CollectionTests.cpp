#include <boost/test/unit_test.hpp>
#include "Amadeus/Traits.h"
#include "Amadeus/GradientMap.h"

BOOST_AUTO_TEST_SUITE(TS_Traits);
BOOST_AUTO_TEST_SUITE(TS_Collection);

BOOST_AUTO_TEST_CASE(CollectionFirst)
{
	{
		using type = collection<1, 2, 3>;
		static_assert(type::first == 1, "collection<1, 2, 3>::first not working");
	}

	{
		using type = collection<3, 2, 1>;
		static_assert(type::first == 3, "collection<3, 2, 1>::first not working");
	}
}

BOOST_AUTO_TEST_CASE(CollectionLast)
{
	{
		using type = collection<1, 2, 3>;
		static_assert(type::last == 3, "collection<1, 2, 3>::last not working");
	}

	{
		using type = collection<3, 2, 1>;
		static_assert(type::last == 1, "collection<3, 2, 1>::last not working");
	}
}

BOOST_AUTO_TEST_CASE(CollectionSize)
{
	{
		using type = collection<1, 2, 3>;
		static_assert(type::size == 3, "collection<1, 2, 3>::size not working");
	}

	{
		using type = collection<3>;
		static_assert(type::size == 1, "collection<3, 2, 1>::last not working");
	}
}

BOOST_AUTO_TEST_CASE(CollectionSubRange)
{
	{
		using type = collection<1, 2, 3, 4>::subrange<1, 2>;
		static_assert(type::size == 2, "collection<1, 2, 3, 4>::subrange<1, 2>::size not working");
		static_assert(type::first == 2, "collection<1, 2, 3, 4>::subrange<1, 2>::first not working");
		static_assert(type::last == 3, "collection<1, 2, 3, 4>::subrange<1, 2>::last not working");
	}

	{
		using type = collection<1, 2, 3, 4>::subrange<3, 1>;
		static_assert(type::size == 1, "collection<1, 2, 3, 4>::subrange<3, 1>::size not working");
		static_assert(type::first == 4, "collection<1, 2, 3, 4>::subrange<3, 1>::first not working");
		static_assert(type::last == 4, "collection<1, 2, 3, 4>::subrange<3, 1>::last not working");
	}
}

BOOST_AUTO_TEST_CASE(CollectionReversed)
{
	{
		using type = collection<1, 2, 3>::reversed;
		static_assert(type::size == 3, "collection<1, 2, 3>::reversed::size not working");
		static_assert(type::first == 3, "collection<1, 2, 3>::reversed::first not working");
		static_assert(type::last == 1, "collection<1, 2, 3>::reversed::last not working");
	}
}

BOOST_AUTO_TEST_CASE(CollectionRemoveFirst)
{
	{
		using type = collection<1, 2, 3>::remove_front<2>;
		static_assert(type::size == 1, "collection<1, 2, 3>::remove_first<2>::size not working");
		static_assert(type::first == 3, "collection<1, 2, 3>::remove_first<2>::first not working");
		static_assert(type::last == 3, "collection<1, 2, 3>::remove_first<2>::last not working");
	}

	{
		using type = collection<9, 2, 5, 2, 5>::remove_front<3>;
		static_assert(type::size == 2, "collection<9, 2, 5, 2, 5>::remove_first<3>::size not working");
		static_assert(type::first == 2, "collection<9, 2, 5, 2, 5>::remove_first<3>::first not working");
		static_assert(type::last == 5, "collection<9, 2, 5, 2, 5>::remove_first<3>::last not working");
	}
}

BOOST_AUTO_TEST_CASE(CollectionRemoveLast)
{
	{
		using type = collection<1, 2, 3>::remove_back<2>;
		static_assert(type::size == 1, "collection<1, 2, 3>::remove_last<2>::size not working");
		static_assert(type::first == 1, "collection<1, 2, 3>::remove_last<2>::first not working");
		static_assert(type::last == 1, "collection<1, 2, 3>::remove_last<2>::last not working");
	}

	{
		using type = collection<9, 2, 5, 2, 5>::remove_back<3>;
		static_assert(type::size == 2, "collection<9, 2, 5, 2, 5>::remove_last<3>::size not working");
		static_assert(type::first == 9, "collection<9, 2, 5, 2, 5>::remove_last<3>::first not working");
		static_assert(type::last == 2, "collection<9, 2, 5, 2, 5>::remove_last<3>::last not working");
	}
}

template<typename ... Ts> struct A {};
template<typename ... Ts> struct B {};

template<typename T>
using map_value_vector = typename empty_parameter_collection
	::from<T>
	::template subrange<1, 1>
	::template transfer_to<std::vector<placeholder>>;

BOOST_AUTO_TEST_CASE(SwapParameters)
{
	{
		using type = empty_parameter_collection
			::from<A<int, float>>
			::remove_back
			::transfer_to<B<>>;

		static_assert(std::is_same_v<type, B<int>>, "transferring paramters didn't work");
	}

	{
		static_assert(std::is_same_v<map_value_vector<std::map<std::string, int>>, std::vector<int>>, "transferring value type to vector didn't work");
	}

	{
		using InputType = std::vector<int>;	
		using type = collections::parameters
         ::from<InputType>
         ::subrange<0, 1>
         ::replace<int, double>
         ::transfer_to<std::vector<placeholder>>;
	
		static_assert(std::is_same_v<type, std::vector<double>>, "NO");
	}

	{
		using InputType = A<int, int, double, int>;
		using TargetType = B<>;

		using OutputType = collections::parameters::from<InputType>::replace<int, char>::transfer_to<TargetType>;
	}
}

template<int...N>
struct seq {};

BOOST_AUTO_TEST_CASE(CollectionTransferTo)
{
	using type = collection<1, 2, 3>::remove_front<1>::remove_back<1>::inject_to<seq<>>;
	static_assert(std::is_same_v<type, seq<2>>, "NO");

	using h = to_matrix_tuple_t<float, 1, 3, 2, 4>;
	using t = Amadeus::AI::GradientMap<2, 2, 2>::DataType;
}

// TODO: SOLVE RECURSIVE DECLARATION OF REVERSED
//BOOST_AUTO_TEST_CASE(ReverseParameters)
//{
//	{
//		using type = parameter_collection<int, float, double>;
//		static_assert(std::is_same_v<type, parameter_collection<double, float, int>>, "reversed didn't work");
//	}
//}

BOOST_AUTO_TEST_SUITE_END();
BOOST_AUTO_TEST_SUITE_END();