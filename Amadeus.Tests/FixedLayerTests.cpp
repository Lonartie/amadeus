// ******************************************************************************************
// Project: Amadeus.Tests
// File:    FixedLayerTests.cpp
// Author:  [ ] .
// Created: 2020-07-12
// Changed: 2020-08-21
// ******************************************************************************************

#include <boost/test/unit_test.hpp>
#include "Amadeus/Amadeus"
using namespace Amadeus::AI;

BOOST_AUTO_TEST_SUITE(TS_Layer);
BOOST_AUTO_TEST_SUITE(TS_FixedLayer);
BOOST_AUTO_TEST_SUITE(Structure);

BOOST_AUTO_TEST_CASE(LayerIsFixedSize)
{
	static_assert(!FixedLayer<3, 4>::IsDynamic, "Layer<3, 4> may not be dynamic!");
	BOOST_CHECK(true);
}

BOOST_AUTO_TEST_SUITE_END();
BOOST_AUTO_TEST_SUITE(Calculation);

BOOST_AUTO_TEST_CASE(Calculation_01)
{
	const FixedLayer<3, 4> layer(3);
	const auto result = layer.run(Eigen::Vector3f::Constant(2));
	const Eigen::Vector4f expected = {21, 21, 21, 21};
	BOOST_TEST(result == expected);
}

BOOST_AUTO_TEST_CASE(Calculation_02)
{
	const FixedLayer<4, 3> layer(3);
	const auto result = layer.run(Eigen::Vector4f::Constant(2));
	const Eigen::Vector3f expected = {27, 27, 27};
	BOOST_TEST(result == expected);
}

BOOST_AUTO_TEST_CASE(Calculation_03)
{
	const Sigmoid sig;
	FixedLayer<4, 3> layer(0.5);
	layer.setFunc([&sig](const float x) {return sig.run(x); });
	const auto result = layer.run(Eigen::Vector4f::Constant(0.3));
	const Eigen::Vector3f expected = {0.750260115F, 0.750260115F, 0.750260115F};
	BOOST_TEST(result == expected);
}

BOOST_AUTO_TEST_SUITE_END();
BOOST_AUTO_TEST_SUITE_END();
BOOST_AUTO_TEST_SUITE_END();