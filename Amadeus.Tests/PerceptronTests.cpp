// ******************************************************************************************
// Project: Amadeus.Tests
// File:    PerceptronTests.cpp
// Author:  [ ] .
// Created: 2020-07-05
// Changed: 2020-08-21
// ******************************************************************************************

#include <boost/test/unit_test.hpp>
#include "benchmark.h"
#include "Amadeus/Amadeus"
#include "Amadeus/MSE.h"
#include "Amadeus/Stochastic.h"

using namespace Amadeus::Networks;
using namespace Amadeus::AI;
using namespace Amadeus::Network::Algorithms;
using namespace Amadeus::Network::Trainer;

BOOST_AUTO_TEST_SUITE(TS_Perceptron);
BOOST_AUTO_TEST_SUITE(Types);

BOOST_AUTO_TEST_CASE(StaticPerceptronTypes)
{
	ANN<4, 3, 3, 4> network;

	auto first = network.layer<0>();
	auto mid = network.layer<1>();
	auto last = network.layer<2>();

	static_assert(std::is_same_v<decltype(first), Layer<4, 3>>, "NO");
	static_assert(std::is_same_v<decltype(mid), Layer<3, 3>>, "NO");
	static_assert(std::is_same_v<decltype(last), Layer<3, 4>>, "NO");

	BOOST_CHECK(true);
}

BOOST_AUTO_TEST_CASE(PerceptronDynamicTypes)
{
	/*constexpr bool is_same = std::is_same_v<Perceptron<Dynamic, Dynamic>, DynamicPerceptron>;
	static_assert(is_same, "Perceptron<Dynamic, Dynamic> and DynamicPerceptron are not the same type!");
	BOOST_CHECK(is_same);

	constexpr bool not_same = !std::is_same_v<Perceptron<1, 2>, DynamicPerceptron>;
	static_assert(not_same, "Perceptron<1, 2> and DynamicPerceptron must not be the same type!");
	BOOST_CHECK(not_same);*/
}

BOOST_AUTO_TEST_CASE(DynamicPerceptronIsDynamic)
{
	/*static_assert(DynamicPerceptron::IsDynamic, "DynamicPerceptron::IsDynamic must be true");
	BOOST_CHECK(DynamicPerceptron::IsDynamic);

	constexpr bool not_dynamic = !Perceptron<1, 2>::IsDynamic;
	static_assert(not_dynamic, "DynamicPerceptron::IsDynamic must be true");
	BOOST_CHECK(not_dynamic);*/
}

BOOST_AUTO_TEST_CASE(DynamicPerceptronSizeConstructor)
{
	//DynamicPerceptron perceptron(1, 2);
	//BOOST_TEST(2, perceptron.layers().size());

	//BOOST_TEST(1, perceptron.layers()[0].neurons());
	//BOOST_TEST(2, perceptron.layers()[1].neurons());
}

BOOST_AUTO_TEST_SUITE_END();
BOOST_AUTO_TEST_SUITE(Behaviour);

BOOST_AUTO_TEST_CASE(DoesNotThrow)
{
	const Perceptron<2, 2> p;
	const Eigen::Vector2f input({0.5, 0.5});

	auto result = p.run(input);
	BOOST_TEST(result.size() == 2);
	BOOST_TEST(result[0] <= 2);
	BOOST_TEST(result[0] >= -2);
	BOOST_TEST(result[1] <= 2);
	BOOST_TEST(result[1] >= -2);
}

BOOST_AUTO_TEST_CASE(RunBench)
{
	const FixedPerceptron<2, 2> p;
	const auto input = Eigen::Vector2f::Random();

	Benchmark bench([&]() { p.run(input); });

#ifdef NDEBUG
	bench.run(1e6);
#else
	bench.run(1e3);
#endif

	BOOST_TEST_MESSAGE("running with ~ " << bench.callsPerSecond() << " calls/s");
	BOOST_TEST_MESSAGE("running with ~ " << bench.formattedTimePerCall());
	BOOST_TEST_MESSAGE("running with min ~ " << bench.formattedMinTimePerCall());
	BOOST_TEST_MESSAGE("running with max ~ " << bench.formattedMaxTimePerCall());
	BOOST_TEST(true);
}

BOOST_AUTO_TEST_CASE(FixedTrainingSingle)
{
	FixedANN<2, 2, 2> network;
	Sigmoid sig;

	network.layer<0>().weights()(0, 0) = 0.15;
	network.layer<0>().weights()(0, 1) = 0.20;
	network.layer<0>().weights()(1, 0) = 0.25;
	network.layer<0>().weights()(1, 1) = 0.30;
	network.layer<0>().bias()(0) = .35;
	network.layer<0>().bias()(1) = .35;
	network.layer<0>().setFunc([&](const float x) { return sig.run(x); });
	network.layer<0>().setDerivative([&](const float x) { return sig.derivative(x); });

	network.layer<1>().weights()(0, 0) = 0.40;
	network.layer<1>().weights()(0, 1) = 0.45;
	network.layer<1>().weights()(1, 0) = 0.50;
	network.layer<1>().weights()(1, 1) = 0.55;
	network.layer<1>().bias()(0) = .60;
	network.layer<1>().bias()(1) = .60;
	network.layer<1>().setFunc([&](const float x) { return sig.run(x); });
	network.layer<1>().setDerivative([&](const float x) { return sig.derivative(x); });

	const auto r = network.run(Vector(.05, .10));
	const auto a = r(0);
	const auto b = r(1);

	BOOST_CHECK_CLOSE_FRACTION(a, 0.75136507, 0.0001);
	BOOST_CHECK_CLOSE_FRACTION(b, 0.77292847, 0.0001);

	TrainingSample<2, 2> sample { Vector(.05, .10), Vector(.01, .99) };

	Stochastic<2, 2, 2> trainer(network);
	trainer.setLossFunction(std::make_unique<MSE>());
	const auto errors = trainer.calculateErrorMap(sample);

	/*BOOST_CHECK_CLOSE_FRACTION(std::get<0>(errors.Data)(0), , 0.0001);
	BOOST_CHECK_CLOSE_FRACTION(std::get<0>(errors.Data)(1), , 0.0001);
	BOOST_CHECK_CLOSE_FRACTION(std::get<0>(errors.Data)(2), , 0.0001);
	BOOST_CHECK_CLOSE_FRACTION(std::get<0>(errors.Data)(3), , 0.0001);*/

	BOOST_CHECK_CLOSE_FRACTION(std::get<1>(errors.Data)[0], 0.35891648, 0.0001);
	BOOST_CHECK_CLOSE_FRACTION(std::get<1>(errors.Data)[1], 0.40866619, 0.0001);
	BOOST_CHECK_CLOSE_FRACTION(std::get<1>(errors.Data)[0], 0.51130127, 0.0001);
	BOOST_CHECK_CLOSE_FRACTION(std::get<1>(errors.Data)[1], 0.56137012, 0.0001);
}

BOOST_AUTO_TEST_CASE(XORTraining)
{
	FixedPerceptron<2, 1> p;
	const TrainingSet<2, 1> samples =
	{{
		{ Vector(0, 0), Vector(0) },
		{ Vector(0, 1), Vector(1) },
		{ Vector(1, 0), Vector(1) },
		{ Vector(1, 1), Vector(0) },
	}};

	Stochastic<2, 1, 1> trainer(p);
	trainer.setLossFunction(std::make_unique<MSE>());
	trainer.train(samples);
}

BOOST_AUTO_TEST_SUITE_END();
BOOST_AUTO_TEST_SUITE_END();