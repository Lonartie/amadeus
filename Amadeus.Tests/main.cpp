// ******************************************************************************************
// Project: Amadeus.Tests
// File:    main.cpp
// Author:  [ ] .
// Created: 2020-07-01
// Changed: 2020-08-21
// ******************************************************************************************

#define BOOST_TEST_MODULE AmadeusTests
#include <boost/test/unit_test.hpp>

#include <Windows.h>

namespace boost
{
	void throw_exception(const std::exception& ex){}
}

namespace
{
	auto st = []
	{
		SetConsoleOutputCP(CP_UTF8);
		return true;
	}();
}